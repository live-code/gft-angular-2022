import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <h1>settings works!</h1>
    
    <button (click)="themeService.theme = 'dark'">dark</button>
    <button (click)="themeService.theme = 'light'">light</button>
  `,
})
export class SettingsComponent  {

  constructor(public themeService: ThemeService) {
  }

}
