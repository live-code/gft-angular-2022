import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { User } from '../../../model/user';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-users',
  template: `
    <div *ngIf="usersService.error">Server problem!</div>
    
    <app-users-form 
      (save)="usersService.addUser($event)"
    ></app-users-form>
    
    <app-users-list
      [users]="usersService.users"
      (deleteUser)="usersService.deleteUser($event)"
    ></app-users-list>
  `,
  providers: [
    UsersService
  ]
})
export class UsersComponent {
  constructor(
    public usersService: UsersService
  ) {
    this.usersService.loadUsers();
  }

}
