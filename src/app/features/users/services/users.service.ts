import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { User } from '../../../../model/user';

@Injectable()
export class UsersService {
  users: User[] = [];
  error: boolean = false;

  constructor( private http: HttpClient ) {}

  loadUsers() {
    this.error = false;

    this.http.get<User[]>(`${environment.baseApi}/users/`)
      .subscribe({
        next: (res) => this.users = res,
        error: () => this.error = true
      })
  }



  deleteUser(idToDelete: number) {
    this.error = false;
    //this.http.delete(environment.baseApi + '/users/' + idToDelete)
    this.http.delete<void>(`${environment.baseApi}/users/${idToDelete}`)
      .subscribe({
        next: () => {
          const index = this.users.findIndex(u => u.id === idToDelete)
          this.users.splice(index, 1);
        },
        error: () => this.error = true
      })
  }

  addUser(formData: any) {
    this.error = false;

    this.http.post<User>(environment.baseApi + '/users', formData)
      .subscribe(res => {
        this.users.push(res)
      })
  }
}
