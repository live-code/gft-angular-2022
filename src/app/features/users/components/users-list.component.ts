import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../../model/user';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-users-list',
  template: `
    <li *ngFor="let user of users">
      {{user.name}} - {{user.phone}}
      <i class="fa fa-trash"
         (click)="deleteUser.emit(user.id)"></i>
    </li>
  `,
})
export class UsersListComponent {
  @Input() users: User[] = [];
  @Output() deleteUser = new EventEmitter<number>();
}
