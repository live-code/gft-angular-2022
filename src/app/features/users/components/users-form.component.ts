import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-users-form',
  template: `
    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input type="text" ngModel name="name" required placeholder="name">
      <input type="text" ngModel name="phone" required placeholder="phone">
      <input type="text" ngModel name="email" required placeholder="email">
      <button type="submit" [disabled]="f.invalid">Add</button>
    </form>
  `,
})
export class UsersFormComponent {
 @Output() save = new EventEmitter()
}
