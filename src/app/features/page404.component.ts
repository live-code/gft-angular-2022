import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page404',
  template: `
    <p>
      This page does not exist.
    </p>
    
    <button routerLink="/home">go to home</button>
  `,
  styles: [
  ]
})
export class Page404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
