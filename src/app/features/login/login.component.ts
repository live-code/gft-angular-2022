import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-login',
  template: `
    <form #f="ngForm" (submit)="login(f)">

      <small *ngIf="f.dirty && inputUsername.errors?.['required']">field is required</small>
      <small *ngIf="f.dirty && inputUsername.errors?.['minlength']">
        Missing {{getMissingChars(inputUsername)}} chars
      </small>
      
      <input 
        type="text" 
        name="username"
        ngModel
        #inputUsername="ngModel"
        required minlength="3"
        placeholder="username" 
        class="form-control"
        [pattern]="ALPHA_NUMERIC_REGEX"
        [ngClass]="{'is-invalid': f.dirty && inputUsername.invalid, 'is-valid': inputUsername.valid}"
      >

      <small *ngIf="f.dirty && inputPassword.errors?.['required']">field is required</small>
      <small *ngIf="f.dirty && inputPassword.errors?.['minlength']">
        Missing {{getMissingChars(inputPassword)}} chars
      </small>

      <input 
        type="text" 
        required minlength="6"
        name="password"
        ngModel
        #inputPassword="ngModel"
        placeholder="pass" class="form-control"
        [ngClass]="{'is-invalid': f.dirty && inputPassword.invalid, 'is-valid': inputPassword.valid}"
      >
      
      <button class="btn btn-primary" [disabled]="f.invalid">LOGIN</button>
    </form>

    <pre>value: {{f.value | json}}</pre>

  `,
})
export class LoginComponent {
  ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

  login(f: NgForm) {
    console.log('login', f.value)
  }

  getMissingChars(input: NgModel) {
    return input.errors?.['minlength']['requiredLength'] - input.errors?.['minlength']['actualLength']
  }
}
