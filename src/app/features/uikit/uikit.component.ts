import { Component, OnInit } from '@angular/core';
import { City, Country } from '../../model/country';

@Component({
  selector: 'app-uikit',
  template: `
    <app-tab-bar
      [active]="activeCountry"
      [items]="countries"
      (tabClick)="selectCountry($event)"
    ></app-tab-bar>
    
    <app-tab-bar
      *ngIf="activeCountry"
      [active]="activeCity"
      [items]="activeCountry.cities"
      (tabClick)="selectCity($event)"
    ></app-tab-bar>

    <app-card 
      *ngIf="activeCity"
      [title]="activeCity.name"
      icon="fa fa-eye"
      (iconClick)="openWiki(activeCity.name)"
    >
      {{activeCity.desc}}
    </app-card>
  `,
})
export class UikitComponent {
  countries: Country[] = [];
  activeCountry: Country | undefined
  activeCity: City | undefined;

  constructor() {
    setTimeout(() => {
      this.countries =  [
        {
          id: 1,
          name: 'Italy',
          cities: [
            { id: 1, name: 'Roma', desc: 'bla bla 1'},
            { id: 2, name: 'Milan', desc: 'bla bla 2'},
          ]
        },
        { id: 2, name: 'Spain', cities: [{ id: 11, name: 'Madrid', desc: 'madrid bla bla'}]  },
        { id: 3, name: 'Germany', cities: [{ id: 21, name: 'Berlin', desc: ' bla bla'}, { id: 22, name: 'Monaco', desc: ' bla bla'}, { id: 23, name: 'Frankfurt',  desc: ' bla bla'} ]  },
      ];
      this.selectCountry(this.countries[0])
    }, 2000)
  }

  selectCountry(country: Country) {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0]
  }

  selectCity(city: City) {
    this.activeCity = city;
  }

  label = 'my profile'
  visible = false;

  openUrl(url: string) {
    window.open(url)
  }

  openWiki(name: string) {
    window.open('https://en.wikipedia.org/wiki/' + name)
  }

}


