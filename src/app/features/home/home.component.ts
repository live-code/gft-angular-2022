import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-home',
  template: `
    <h1>home</h1>

    <div class="container">
      <input
        #inputName
        type="text" class="form-control" placeholder="add user name"
        (keydown.enter)="keydownHandler()"
      >
     <input
        #inputPhone
        type="text" class="form-control" placeholder="add user phone"
        (keydown.enter)="keydownHandler()"
      >
      
      <ul class="list-group">
        <li class="list-group-item" *ngFor="let user of users">
          <div class="d-flex justify-content-between align-items-center">
            <h1>{{user?.name}} - {{user.phone}}</h1>
            <i class="fa fa-trash" (click)="deleteUser(user.id)"></i>
          </div>
        </li>
      </ul>
    </div>
  `
})
export class HomeComponent {
  @ViewChild('inputName') name!: ElementRef<HTMLInputElement>
  @ViewChild('inputPhone') phone!: ElementRef<HTMLInputElement>

  ngAfterViewInit() {
    this.name.nativeElement.focus();
  }

  users: User[] = [
    {
      id: 1,
      name: 'Fabio',
      phone: 123
    },
    {
      id: 2,
      name: 'SIlvia',
      phone: 456
    },
    {
      id: 3,
      name: 'Lisa',
      phone: 789
    }
  ]

  deleteUser(id: number) {
    const index = this.users.findIndex(u => u.id === id)
    this.users.splice(index, 1)
  }

  keydownHandler() {
    this.users.push({
      id: Date.now(),
      name: this.name.nativeElement.value,
      phone: +this.phone.nativeElement.value
    })
    this.name.nativeElement.value = ''
    this.phone.nativeElement.value = ''
  }

  ngOnDestroy() {
    console.log('destroy')
  }
}
