import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <p [style.color]="color">
      hello {{name}}
    </p>
  `,
})
export class HelloComponent {
  @Input() name = 'Mario'
  @Input() color = 'red'
}
