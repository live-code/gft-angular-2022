import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `
    <div class="card">
      <div class="card-body">
        
        <h5 class="card-title" (click)="isOpened = !isOpened">
          <div
            class="d-flex justify-content-between align-items-center"
          >
            <div>{{title}}</div>
            <i *ngIf="icon" [class]="icon"
               (click)="doAction($event)"></i>
          </div>
        </h5>
        
        <div class="card-text"  *ngIf="isOpened">
          <ng-content></ng-content>
        </div>
        
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: any
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter();
  isOpened = false;

  doAction(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit();
  }
}
