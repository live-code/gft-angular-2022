import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './features/home/home.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { LoginComponent } from './features/login/login.component';
import { UsersComponent } from './features/users/users.component';
import { Page404Component } from './features/page404.component';
import { SettingsComponent } from './features/settings/settings.component';
import { NavbarComponent } from './core/components/navbar.component';
import { HelloComponent } from './shared/hello.component';
import { UikitComponent } from './features/uikit/uikit.component';
import { CardComponent } from './shared/card.component';
import { TabBarComponent } from './shared/tab-bar.component';
import { UsersListComponent } from './features/users/components/users-list.component';
import { UsersFormComponent } from './features/users/components/users-form.component';
import { UsersListItemComponent } from './features/users/components/users-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactsComponent,
    LoginComponent,
    UsersComponent,
    Page404Component,
    SettingsComponent,
    NavbarComponent,
    HelloComponent,
    UikitComponent,
    CardComponent,
    TabBarComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersListItemComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: 'home', component: HomeComponent },
      { path: 'login', component: LoginComponent },
      { path: 'contacts', component: ContactsComponent },
      { path: 'users', component: UsersComponent },
      { path: 'uikit', component: UikitComponent },
      { path: 'settings', component: SettingsComponent },
      { path: '404', component: Page404Component },
      { path: '', redirectTo: 'home', pathMatch: 'full'},
      { path: '**', redirectTo: '404' },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
