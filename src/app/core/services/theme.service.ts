import { Injectable } from '@angular/core';
import { Theme } from '../../model/theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private _theme: Theme = 'dark';

  constructor() {
    const themeFromLocalStorage = localStorage.getItem('theme');
    if (themeFromLocalStorage) {
      this._theme = themeFromLocalStorage as Theme;
    }
  }

  set theme(newTheme: Theme) {
    localStorage.setItem('theme', newTheme)
    this._theme = newTheme;
  }

  get theme(): Theme {
    return this._theme
  }
}
