import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../features/users/services/users.service';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      class="p-4" 
      [ngClass]="{
        'bg-dark': themeService.theme === 'dark',
        'bg-info': themeService.theme === 'light'
      }"
    >
      <div class="btn-group">
        <button routerLink="home" class="btn btn-primary"
                [routerLinkActiveOptions]="{exact: true }"
                routerLinkActive="activeItem">home</button>
        <button routerLink="settings" class="btn btn-primary" routerLinkActive="activeItem">settings</button>
        <button routerLink="login" class="btn btn-primary" routerLinkActive="activeItem">login</button>
        <button routerLink="uikit" class="btn btn-primary" routerLinkActive="activeItem">uikit</button>
        <button routerLink="contacts" class="btn btn-primary" routerLinkActive="activeItem">contacts</button>
        <button routerLink="users" class="btn btn-primary" routerLinkActive="activeItem">users</button>
      </div>
    </div>
  `,
  styles: [`
    .activeItem {
      background-color: red;
    }
  `]
})
export class NavbarComponent {
  constructor(
    public themeService: ThemeService,
  ) {

  }

  doSomething() {
    this.themeService.theme
  }
}
